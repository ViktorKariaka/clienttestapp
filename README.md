Client Test Application
========================

(!)Make sure that the server application is configured and running
--------------

Install application:
--------------

```
composer install
```

Setup in config/parameters.yml:

+ Path to .xml config
+ URL to remote server

Run command for send data to the server:

```
php bin/console app:send-data
```
