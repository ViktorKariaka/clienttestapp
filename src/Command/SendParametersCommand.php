<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use App\Exception\FileNotFoundException;
use GuzzleHttp\Client;
use Exception;

/**
 * Class SendParametersCommand
 *
 * @package App\Command
 */
final class SendParametersCommand extends Command
{
    /**
     * Command name
     */
    private const SEND_XML_PARAMETERS_CMD_NAME = 'app:send-data';

    /**
     * Application config
     *
     * @var array
     */
    private $config;

    /**
     * SendXMLParametersCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->config = Yaml::parseFile('./config/parameters.yml');
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this->setName(self::SEND_XML_PARAMETERS_CMD_NAME);
        $this->setDescription('Send XML parameters');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $parsedConfig = $this->getParsedConfig();
            $responseContent = $this->sendConfig($parsedConfig);

            if ($responseContent === '0') {
                $output->writeln('Success');
            } else {
                $output->writeln('Error');
            }
        } catch (FileNotFoundException $exception) {
            $output->writeln($exception->getMessage());
        } catch (Exception $exception) {
            $output->writeln($exception->getMessage());
        }
    }

    /**
     * @return array
     *
     * @throws FileNotFoundException
     */
    private function getParsedConfig(): array
    {
        $path = $this->config['parameters']['path_to_xml_config_file'];

        if (file_exists($path)) {
            /** @var \SimpleXMLElement $xml */
            $xml = simplexml_load_file($path);
            $texts = $xml->{'texts'};
            $parsedText = [];

            foreach ($texts->attributes() as $parameter => $value) {
                $parsedText[$parameter] = $value->__toString();
            }

            return $parsedText;
        }

        throw new FileNotFoundException('File ' . $path . ' not found!');
    }

    /**
     * @param array $jsonConfig
     *
     * @return string
     */
    private function sendConfig(array $jsonConfig): string
    {
        $url = $this->config['parameters']['url_to_remote_server'];

        $client = new Client();

        $options = [
            'body' => json_encode($jsonConfig)
        ];

        $response = $client->post($url, $options);

        return $response->getBody()->getContents();
    }
}
